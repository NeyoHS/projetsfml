#include "mur.hpp"
#include "deplacement.hpp"
#include <iostream>
#include <sstream>
#include <SFML/Network.hpp>
#include <SFML/Graphics.hpp>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include "accueil.hpp"
#include "local.hpp"
#include "score.hpp"
#include "tir.hpp"

//Define des constantes de taille
#define LARG_FEN 1600
#define HAUT_FEN 900
#define LARG_MAIN 700
#define HAUT_MAIN 200
#define LARG_BOUTON 500
#define HAUT_BOUTON 100
#define ECART_BOUTON 150
#define PAUSE 10


using namespace sf;


const unsigned short PORT = 5000;
const IpAddress IPADDRESS("10.103.60.7"); //change to suit your needs

TcpSocket socket;
Mutex globalMutex;
bool quit = false;

using namespace std;
//using namespace std;

//Structure de position

Tank tRouge; // On definit le tank rouge
Tank tBleu; // On definit le tank bleu
Tank bRouge;
Tank bBleu;

int gestionBouletB = 0;
int feuBouletB = 0;
int timeBouletB = 0;

int gestionBouletR = 0;
int feuBouletR = 0;
int timeBouletR = 0;

int pasXBouletBleu;
int pasYBouletBleu;
int pasXBouletRouge;
int pasYBouletRouge;

int scoreR=0;
int scoreB=0;

int close = 0;

int spawn;

Texture tankrouge;
Texture tankbleu;

Texture boulet;
Sprite bouletR, bouletB;


Packet packetSend;

Texture sol,mur,tankTexture;
RectangleShape tankR(Vector2f(TAILLE_TANK,TAILLE_TANK));
RectangleShape tankB(Vector2f(TAILLE_TANK,TAILLE_TANK));
RectangleShape grille[NB_CASE_LONGUEUR][NB_CASE_LARGEUR];

typedef struct
{
    int x;
    int y;
} Position;

using namespace std;



void Server(void)
{
    TcpListener listener;
    listener.listen(PORT);
    listener.accept(socket);
    cout << "New client connected: " << socket.getRemoteAddress() << endl;
}

bool Client(void)
{
    if(socket.connect(IPADDRESS, PORT) == Socket::Done)
    {
        cout << "Connected\n";
        return true;
    }
    return false;
}

int accueil()
{
//Rectangle de selection des menus
    RectangleShape rect(Vector2f(LARG_BOUTON+20, HAUT_BOUTON+20));
    rect.setFillColor(Color::White);

//Process des events
    Event event;

//Initialisation de la position des menus
    Position coordSouris = {0,0};
    Position coordMainMenu = {(LARG_FEN/2)-(LARG_MAIN/2),100};
    Position coordLocalMenu = {(LARG_FEN/2)-(LARG_BOUTON/2),HAUT_MAIN+ECART_BOUTON};
    Position coordMultiMenu = {(LARG_FEN/2)-(LARG_BOUTON/2),HAUT_MAIN+ECART_BOUTON*2};
    Position coordHebergerMenu = {(LARG_FEN/2)-(LARG_BOUTON/2),HAUT_MAIN+ECART_BOUTON};
    Position coordRejoindreMenu = {(LARG_FEN/2)-(LARG_BOUTON/2),HAUT_MAIN+ECART_BOUTON*2};
    Position coordRetourMenu = {(LARG_FEN/2)-(LARG_BOUTON/2),HAUT_MAIN+ECART_BOUTON*3};


//Variables de gestion des menus
    int choixMenu = 1;
    int afficheMenuDeux = 0;
    int setRect = 0;



// Fenetre
    RenderWindow app(VideoMode(LARG_FEN, HAUT_FEN), "World of tanks", Style::Fullscreen);
    app.clear(Color::Black);

    ///Musique du menu///
    Music menu;
    menu.openFromFile("menu.wav");
    menu.play();

// Chargement des images du menu
    Texture imageMainMenu;
    imageMainMenu.loadFromFile("Main_Menu.png");
    Texture imageLocalMenu;
    imageLocalMenu.loadFromFile("Local_Menu.png");
    Texture imageMultiMenu;
    imageMultiMenu.loadFromFile("Multi_Menu.png");
    Texture imageHebergerMenu;
    imageHebergerMenu.loadFromFile("Heberger_Menu.png");
    Texture imageRejoindreMenu;
    imageRejoindreMenu.loadFromFile("Rejoindre_Menu.png");
    Texture imageBackgroundMenu;
    imageBackgroundMenu.loadFromFile("Background_menu.jfif");
    Texture imageRetour;
    imageRetour.loadFromFile("Retour_Menu.png");

//Cr�ation des sprites
    Sprite spriteMainMenu;
    spriteMainMenu.setTexture(imageMainMenu);
    Sprite spriteLocalMenu;
    spriteLocalMenu.setTexture(imageLocalMenu);
    Sprite spriteMultiMenu;
    spriteMultiMenu.setTexture(imageMultiMenu);
    Sprite spriteHebergerMenu;
    spriteHebergerMenu.setTexture(imageHebergerMenu);
    Sprite spriteRejoindreMenu;
    spriteRejoindreMenu.setTexture(imageRejoindreMenu);
    Sprite spriteBackgroundMenu;
    spriteBackgroundMenu.setTexture(imageBackgroundMenu);
    spriteBackgroundMenu.setScale(1.5625,1.171875);
    Sprite spriteRetour;
    spriteRetour.setTexture(imageRetour);

//Position des sprites
    spriteMainMenu.setPosition(coordMainMenu.x,coordMainMenu.y);
    spriteLocalMenu.setPosition(coordLocalMenu.x,coordLocalMenu.y);
    spriteMultiMenu.setPosition(coordMultiMenu.x,coordMultiMenu.y);
    spriteHebergerMenu.setPosition(coordHebergerMenu.x,coordHebergerMenu.y);
    spriteRejoindreMenu.setPosition(coordRejoindreMenu.x,coordRejoindreMenu.y);
    spriteRetour.setPosition(coordRetourMenu.x,coordRetourMenu.y);

//Boucle du jeu
    while (app.isOpen())
    {
        while (app.pollEvent(event))
        {
            switch(event.type)
            {

            //Event de fermeture de la fenetre
            case Event::Closed:
                app.close();

            case Event::KeyPressed:
            {
                //Events de defilement dans les menus
                if((event.key.code == Keyboard::Down)&&(choixMenu<3))
                {
                    choixMenu++;
                    printf("%i ",choixMenu);
                }

                if((event.key.code == Keyboard::Up)&&(choixMenu>1))
                {
                    choixMenu--;
                    printf("%i ",choixMenu);
                }

                //Event de selections des menus
                if(event.key.code == Keyboard::Return)
                {
                    //Selection Jeu en local
                    if(choixMenu == 1 && afficheMenuDeux == 0)
                    {
                        //
                        //  JEU EN LOCAL
                        //
                        printf("local ");
                        app.close();
                        menu.stop();
                        local();
                        close = 1;
                    }

                    //Selection multijoueur
                    else if(choixMenu == 2 && afficheMenuDeux == 0)
                    {
                        afficheMenuDeux = 1;
                    }

                    //Selection heberger
                    else if ((choixMenu == 1) && (afficheMenuDeux == 1))
                    {
                        //
                        //  JEU EN HEBERGEUR
                        //
                        printf("hebergement ");
                        Server();
                        spawn = 1;
                        app.close();
                        menu.stop();


                    }

                    //Selection rejoindre
                    else if((choixMenu == 2) && (afficheMenuDeux == 1))
                    {
                        //
                        //  REJOINDRE UN JEU
                        //
                        printf("rejoindre ");
                        Client();
                        spawn = 2;
                        app.close();
                        menu.stop();


                    }

                    //Retour depuis menu multijoueur
                    else if ((choixMenu == 3) && (afficheMenuDeux == 1))
                    {
                        afficheMenuDeux = 0;
                        choixMenu = 1;
                        printf("retour ");
                    }

                    //Quitter le jeu
                    else if ((choixMenu == 3) && (afficheMenuDeux == 0))
                    {

                        app.close();
                        close = 1;

                        printf("exit ");
                    }
                }
            }

            //Event de gestion de la position de la souris
            case Event::MouseMoved:
            {
                coordSouris.x = event.mouseMove.x;
                coordSouris.y = event.mouseMove.y;
            }

            }
        }

        //Placement du rectangle de choix
        if(choixMenu == 2)
        {
            setRect = 2;
        }
        else if(choixMenu == 1)
        {
            setRect = 1;
        }
        else if(choixMenu == 3)
        {
            setRect = 3;
        }

        if  (setRect == 1)
        {
            rect.setPosition(coordLocalMenu.x-10, coordLocalMenu.y-10);
        }
        else if (setRect == 2)
        {
            rect.setPosition(coordMultiMenu.x-10, coordMultiMenu.y-10);
        }
        else if (setRect == 3)
        {
            rect.setPosition(coordRetourMenu.x-10, coordRetourMenu.y-10);
        }

        app.draw(spriteBackgroundMenu);
        app.draw(rect);
        app.draw(spriteMainMenu);
        app.draw(spriteLocalMenu);
        app.draw(spriteMultiMenu);
        if (afficheMenuDeux == 1)
        {
            app.draw(spriteHebergerMenu);
            app.draw(spriteRejoindreMenu);
        }
        app.draw(spriteRetour);
        app.display();
        sleep( milliseconds(PAUSE));
        app.clear();
    }
    printf("%i ",choixMenu);
}


void DoStuff(void)
{

    boulet.loadFromFile(SOURCE_BOULET);

    bouletR.setTexture(boulet);
    bouletB.setTexture(boulet);

    // On charge l'image du tank rouge dans une texture

    Texture tankrouge;
    if (!tankrouge.loadFromFile(SOURCE_TANK_ROUGE)) // Si probl�me dans le chargement
        printf("PB de chargement de l'image !\n");

    Texture tankbleu;
    if (!tankbleu.loadFromFile(SOURCE_TANK_BLEU)) // Si probl�me dans le chargement
        printf("PB de chargement de l'image !\n");

    sol.loadFromFile(SOURCE_SOL);
    mur.loadFromFile(SOURCE_MUR);


    tankR.setTexture(&tankrouge); // On donne une forme � l'image du tank rouge
    tankR.setPosition(tRouge.posX,tRouge.posY); // On initialise la position du tank rouge
    tankR.setOrigin(TAILLE_TANK/2,TAILLE_TANK/2); // On d�finit la nouvelle origine du tank rouge


    tankB.setTexture(&tankbleu); // On donne une forme � l'image du tank bleu
    tankB.setPosition(tBleu.posX,tBleu.posY); // On initialise la position du tank bleu
    tankB.setOrigin(TAILLE_TANK/2,TAILLE_TANK/2); // On d�finit la nouvelle origine du tank bleu


//Cr�er les composants de la matrice

    creerGrille(grille,sol);
    chargerCarte(grille,1,mur);



    tBleu.posX=LONGUEUR_FENETRE/2; // On initialise la position en abscisse du tank bleu
    tBleu.posY=LARGEUR_FENETRE/2; // On initialise la position en ordonnee du tank bleu
    tRouge.posX=LONGUEUR_FENETRE/3; // On initialise la position en abscisse du tank rouge
    tRouge.posY=LARGEUR_FENETRE/3; // On initialise la position en ordonnee du tank rouge


    RenderWindow app(VideoMode(LONGUEUR_FENETRE, LARGEUR_FENETRE), "World of tank", Style::Fullscreen);


// On charge l'image du tank bleu dans une texture
    Text textJoueurR, textJoueurB, textScore, textScoreR, textScoreB;

    Font font1 = chargeFont1();
    Font font2 = chargeFont2();

    textScore.setString("Scores");
    textScore.setFont(font1);
    textScore.setPosition(130,10);
    textScore.setStyle(Text::Underlined);
    textScore.setCharacterSize(40);

    char ch1[50];
    textScoreR.setFont(font2);
    textScoreR.setPosition(10,100);
    textScoreR.setCharacterSize(30);
    sprintf(ch1,"Tank rouge : %i",scoreR);
    textScoreR.setString(ch1);

    char ch2[50];
    textScoreB.setFont(font2);
    textScoreB.setPosition(10,200);
    textScoreB.setCharacterSize(30);
    sprintf(ch2,"Tank bleu : %i",scoreB);
    textScoreB.setString(ch2);





    Event event;
    //app.draw(tankR);
    //app.display();

    int xReceive = 0, yReceive = 0;
    int rReceive = 255;
    int ancienScoreB = 0;


    while(!quit)
    {

        grilleDraw(grille,app,tankTexture);

        app.clear(); // la fenetre est effacee
        grilleDraw(grille,app,tankTexture);
        app.draw(tankR); // la fen�tre dessine le tank rouge
        app.draw(tankB); // la fen�tre dessine le tank bleu
        app.draw(textScore);
        app.draw(textJoueurB);
        app.draw(textJoueurR);
        app.draw(textScoreB);
        app.draw(textScoreR);
        app.draw(bouletR);
        app.draw(bouletB);
        deplacementTankR(tankR,tRouge,grille,mur); // on appelle la procedure deplacementTankR pour deplacer le tank rouge
        //deplacementTankB(tankB,tBleu); // on appelle la procedure deplacementTankB pour deplacer le tank bleu



        Packet packetSend;
        packetSend.clear();
        packetSend << 1 << tRouge.posX << tRouge.posY << tRouge.rota;
        socket.send(packetSend);


        packetSend.clear();
        packetSend << 2 << bouletR.getPosition().x << bouletR.getPosition().y << bRouge.rota;
        socket.send(packetSend);

        packetSend.clear();
        packetSend << 3 << scoreR  ;
        socket.send(packetSend);


        // Process events
        while (app.pollEvent(event))
        {
            if (event.type == Event::Closed) // si fermeture de fen�tre d�clench�
                app.close();

            if((event.key.code == Keyboard::Space)&&(timeBouletB == 0))
            {
                gestionBouletR = 1;
            }


        }

        if(gestionBouletB == 1)
        {
            bBleu.rota = tBleu.rota;
            bBleu.posX = tBleu.posX + ((TAILLE_TANK/2)+TAILLE_CASE)*cos(convertionDegreRad(bBleu.rota + 90*3));
            bBleu.posY = tBleu.posY + ((TAILLE_TANK/2)+TAILLE_CASE)*sin(convertionDegreRad(bBleu.rota + 90*3));
            pasXBouletBleu = cos(convertionDegreRad(bBleu.rota + 90*3))*BOULET_ACC;
            pasYBouletBleu = sin(convertionDegreRad(bBleu.rota + 90*3))*BOULET_ACC;
            feuBouletB = 1;
            gestionBouletB = 0;
        }
        else
        {
            bBleu.posX = 0;
            bBleu.posY = 0;
            bouletB.setPosition(-10,0);

        }

        if(feuBouletB == 1)
        {
            if(collisionBoulet(bBleu.posX,bBleu.posY,grille,mur,sol) == 1)
                pasXBouletBleu=-pasXBouletBleu;
            if(collisionBoulet(bBleu.posX,bBleu.posY,grille,mur,sol) == 2)
                pasXBouletBleu=-pasXBouletBleu;
            if(collisionBoulet(bBleu.posX,bBleu.posY,grille,mur,sol) == 3)
                pasYBouletBleu=-pasYBouletBleu;
            if(collisionBoulet(bBleu.posX,bBleu.posY,grille,mur,sol) == 4)
                pasYBouletBleu=-pasYBouletBleu;

            bBleu.posX+=pasXBouletBleu;
            bBleu.posY+=pasYBouletBleu;
            bouletB.setPosition(bBleu.posX,bBleu.posY);

            if(touche(bBleu.posX,bBleu.posY,tBleu.posX,tBleu.posY,tRouge.posX,tRouge.posY) == 1)
            {
                timeBouletB = DUREE_BOULET;
                scoreR = changeScoreR(app, textScoreR, scoreR, ch1);
                //ins�rer code score avec joueur rouge gagnant
                printf("\nContact1");
                tBleu.posX=9*LONGUEUR_FENETRE/10; // On initialise la position en abscfse du tank bleu
                tBleu.posY=8*LARGEUR_FENETRE/10; // On initialise la position en ordonnee du tank bleu
                tRouge.posX=3*LONGUEUR_FENETRE/10; // On initialise la position en abscisse du tank rouge
                tRouge.posY=2*LARGEUR_FENETRE/10; // On initialise la position en ordonnee du tank rouge
                if (spawn == 1)
                    tankB.setPosition(tBleu.posX,tBleu.posY);
                if (spawn == 2)
                    tankB.setPosition(tRouge.posX,tRouge.posY);
            }
            else if(touche(bBleu.posX,bBleu.posY,tBleu.posX,tBleu.posY,tRouge.posX,tRouge.posY) == 2)
            {
                timeBouletB = DUREE_BOULET;
                scoreB = changeScoreB( textScoreB, scoreB, ch2);
                //ins�rer code score avec joueur bleu gagnant
                printf("\nContac2t");
                tBleu.posX=9*LONGUEUR_FENETRE/10; // On initialise la position en abscisse du tank bleu
                tBleu.posY=8*LARGEUR_FENETRE/10; // On initialise la position en ordonnee du tank bleu
                tRouge.posX=3*LONGUEUR_FENETRE/10; // On initialise la position en abscisse du tank rouge
                tRouge.posY=2*LARGEUR_FENETRE/10; // On initialise la position en ordonnee du tank rouge
                if (spawn == 1)
                    tankB.setPosition(tBleu.posX,tBleu.posY);
                if (spawn == 2)
                    tankB.setPosition(tRouge.posX,tRouge.posY);
            }

            if(timeBouletB >= DUREE_BOULET)
            {
                timeBouletB = 0;
                feuBouletB = 0;
            }
            else
            {
                timeBouletB++;
            }
        }

        if(gestionBouletR == 1)
        {
            bRouge.rota = tRouge.rota;
            bRouge.posX = tRouge.posX + ((TAILLE_TANK/2)+TAILLE_CASE)*cos(convertionDegreRad(bRouge.rota + 90*3));
            bRouge.posY = tRouge.posY + ((TAILLE_TANK/2)+TAILLE_CASE)*sin(convertionDegreRad(bRouge.rota + 90*3));
            pasXBouletRouge = cos(convertionDegreRad(bRouge.rota + 90*3))*BOULET_ACC;
            pasYBouletRouge = sin(convertionDegreRad(bRouge.rota + 90*3))*BOULET_ACC;
            feuBouletR = 1;
            gestionBouletR = 0;
        }
        else
        {
           // bRouge.posX = 0;
          //  bRouge.posY = 0;
            bouletR.setPosition(-10,0);

        }


        if(feuBouletR == 1)
        {
            if(collisionBoulet(bRouge.posX,bRouge.posY,grille,mur,sol) == 1)
                pasXBouletRouge=-pasXBouletRouge;
            if(collisionBoulet(bRouge.posX,bRouge.posY,grille,mur,sol) == 2)
                pasXBouletRouge=-pasXBouletRouge;
            if(collisionBoulet(bRouge.posX,bRouge.posY,grille,mur,sol) == 3)
                pasYBouletRouge=-pasYBouletRouge;
            if(collisionBoulet(bRouge.posX,bRouge.posY,grille,mur,sol) == 4)
                pasYBouletRouge=-pasYBouletRouge;

            bRouge.posY+=pasYBouletRouge;
            bRouge.posX+=pasXBouletRouge;
            bouletR.setPosition(bRouge.posX,bRouge.posY);

            if(touche(bRouge.posX,bRouge.posY,tBleu.posX,tBleu.posY,tRouge.posX,tRouge.posY) == 1)
            {
                timeBouletR = DUREE_BOULET;
                scoreR = changeScoreR(app, textScoreR, scoreR, ch1);
                //ins�rer code score avec joueur rouge gagnant
                printf("\nContact3");
                tBleu.posX=9*LONGUEUR_FENETRE/10; // On initialise la position en abscisse du tank bleu
                tBleu.posY=8*LARGEUR_FENETRE/10; // On initialise la position en ordonnee du tank bleu
                tRouge.posX=3*LONGUEUR_FENETRE/10; // On initialise la position en abscisse du tank rouge
                tRouge.posY=2*LARGEUR_FENETRE/10; // On initialise la position en ordonnee du tank rouge
                if (spawn == 1)
                    tankR.setPosition(tBleu.posX,tBleu.posY);
                if (spawn == 2)
                    tankR.setPosition(tRouge.posX,tRouge.posY);
            }
            else if(touche(bRouge.posX,bRouge.posY,tBleu.posX,tBleu.posY,tRouge.posX,tRouge.posY) == 2)
            {
                timeBouletR = DUREE_BOULET;
                scoreB = changeScoreB( textScoreB, scoreB, ch2);
                //ins�rer code score avec joueur bleu gagnant
                printf("\nContact4");
                tBleu.posX=9*LONGUEUR_FENETRE/10; // On initialise la position en abscisse du tank bleu
                tBleu.posY=8*LARGEUR_FENETRE/10; // On initialise la position en ordonnee du tank bleu
                tRouge.posX=3*LONGUEUR_FENETRE/10; // On initialise la position en abscisse du tank rouge
                tRouge.posY=2*LARGEUR_FENETRE/10; // On initialise la position en ordonnee du tank rouge
                if (spawn == 1)
                    tankR.setPosition(tBleu.posX,tBleu.posY);
                if (spawn == 2)
                    tankR.setPosition(tRouge.posX,tRouge.posY);
            }



            if(timeBouletR >= DUREE_BOULET)
            {
                timeBouletR = 0;
                feuBouletR = 0;
            }
            else
                timeBouletR++;
        }
        if (ancienScoreB != scoreB) {
                scoreB = changeScoreB( textScoreB, --scoreB, ch2);
            }
            ancienScoreB = scoreB;

        app.display(); // la fen�tre affiche ses dessins
        sleep(milliseconds(PAUSE)); // on fait une pause
    }
}

void receive(void)
{


    while(!quit)
    {
        //static string oldMsg;
        Packet packetReceive;
        //string msg;
        int type;

        socket.receive(packetReceive);
        packetReceive >> type;

        if (type==1) {
            packetReceive >> tBleu.posX >> tBleu.posY >> tBleu.rota;
            cout << tBleu.posX << " " << tBleu.posY<< " " << tRouge.rota << endl;

            tankB.setPosition(tBleu.posX, tBleu.posY);
            tankB.setRotation(tBleu.rota);
        }
        if (type == 2) {
            packetReceive >> bBleu.posX >> bBleu.posY >> bBleu.rota;
            bouletB.setPosition(bBleu.posX,bBleu.posY);
            bouletB.setRotation(tBleu.rota);
        }
        if (type == 3){
            packetReceive >> scoreB;
        }

    }

}



int main(int argc, char* argv[])
{

    //srand(time(NULL));

    Thread* thread = 0; // The thread for the function DoStuff
    Thread* receiveThread = 0; // The thread for the function DoStuff

    /*char who;
    cout << "Do you want to be a server (s) or a client (c) ? ";
    cin  >> who;*/
    accueil();



    /*if (heberge == 1)
    {
        //graphThread = new Thread(&graph, circle);
        //graphThread->launch();
        Server();
    }
    else if (heberge == 0)
        Client();*/

    if (close !=1){
        thread = new Thread(&DoStuff);
        thread->launch();
    }


    receiveThread = new Thread(&receive);
    receiveThread->launch();


    //while (!quit)
    //{
    //    GetInput();
    //}

    if(thread)
    {
        thread->wait();
        delete thread;
    }

    return 0;
}


