#include <SFML/Graphics.hpp>
#include <stdlib.h>

#define SOURCE_MUR "Images-Projet\\Pattern_Mur.png"
#define SOURCE_SOL "Images-Projet\\Pattern_Sol.png"
#define SOURCE_TANK_BLEU "Images-Projet\\Tank_Bleu.png"
#define SOURCE_TANK_ROUGE "Images-Projet\\Tank_Rouge.png"
#define LONGUEUR_FENETRE 1600
#define LARGEUR_FENETRE 900
#define TAILLE_CASE 15
#define NB_CASE_LONGUEUR LONGUEUR_FENETRE*3/4/TAILLE_CASE
#define NB_CASE_LARGEUR LARGEUR_FENETRE/TAILLE_CASE
#define ORIGINE_X_CARTE LONGUEUR_FENETRE/4
#define FPSLIMIT 60
#define TAILLE_TANK 30

using namespace sf;

void creerGrille(RectangleShape grille[][NB_CASE_LARGEUR],Texture &texture);

void creerMurH(int posxOrigine, int posxFin, int posy, RectangleShape grille[][NB_CASE_LARGEUR],Texture &texture);

void creerMurV(int posyOrigine, int posyFin, int posx, RectangleShape grille[][NB_CASE_LARGEUR],Texture &texture);

void grilleDraw(RectangleShape grille[][NB_CASE_LARGEUR], RenderWindow &fenetre,Texture &texture);

void chargerCarte(RectangleShape grille[][NB_CASE_LARGEUR], int numCarte,Texture &texture);

int collision(int posx, int posy, RectangleShape grille[][NB_CASE_LARGEUR],Texture &mur);
