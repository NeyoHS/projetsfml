#include "tir.hpp"

int collisionBoulet(int posx, int posy, RectangleShape grille[][NB_CASE_LARGEUR],Texture &mur,Texture &sol)
{
    int res = 0;
    int numCaseY = (posy)/TAILLE_CASE;
    int numCaseX = (posx)/TAILLE_CASE-ORIGINE_X_CARTE/TAILLE_CASE;

    if((grille[numCaseX][numCaseY].getTexture() == &mur)&&(grille[numCaseX][numCaseY+1].getTexture() == &mur)) //gauhe
        return 1;
    else if((grille[numCaseX][numCaseY].getTexture() == &mur)&&(grille[numCaseX][numCaseY-1].getTexture() == &sol))
        res = 1;
    else if((grille[numCaseX][numCaseY+1].getTexture() == &mur)&&(grille[numCaseX][numCaseY+2].getTexture() == &sol))
        res = 1;

    if((grille[numCaseX+1][numCaseY].getTexture() == &mur)&&(grille[numCaseX+1][numCaseY+1].getTexture() == &mur)) //droite
        return 2;
    else if((grille[numCaseX+1][numCaseY].getTexture() == &mur)&&(grille[numCaseX+1][numCaseY-1].getTexture() == &sol))
        res = 2;
    else if((grille[numCaseX+1][numCaseY+1].getTexture() == &mur)&&(grille[numCaseX+1][numCaseY+2].getTexture() == &sol))
        res = 2;

    if((grille[numCaseX][numCaseY+1].getTexture() == &mur)&&(grille[numCaseX+1][numCaseY+1].getTexture() == &mur)) //bas
        return 3;
    else if((grille[numCaseX][numCaseY+1].getTexture() == &mur)&&(grille[numCaseX-1][numCaseY+1].getTexture() == &sol)) ///////
        res = 3;
    else if((grille[numCaseX+1][numCaseY+1].getTexture() == &mur)&&(grille[numCaseX+2][numCaseY+1].getTexture() == &sol))
        res = 3;

    if((grille[numCaseX][numCaseY].getTexture() == &mur)&&(grille[numCaseX+1][numCaseY].getTexture() == &mur)) //haut
        return 4;
    else if((grille[numCaseX][numCaseY].getTexture() == &mur)&&(grille[numCaseX-1][numCaseY].getTexture() == &sol))
        res = 4;
    else if((grille[numCaseX+1][numCaseY].getTexture() == &mur)&&(grille[numCaseX+2][numCaseY].getTexture() == &sol))
        res = 4;

    return res;
}

int touche(int bouletX,int bouletY,int tankBX,int tankBY,int tankRX,int tankRY)
{
    int res = 0;

    if(bouletY < tankBY+TAILLE_TANK/2 && bouletY+TAILLE_BOULET > tankBY-TAILLE_TANK/2 && bouletX+TAILLE_BOULET > tankBX-TAILLE_TANK/2 && bouletX <tankBX+TAILLE_TANK/2)
        res = 1;
    else if(bouletY < tankRY+TAILLE_TANK/2 && bouletY+TAILLE_BOULET > tankRY-TAILLE_TANK/2 && bouletX+TAILLE_BOULET > tankRX-TAILLE_TANK/2 && bouletX <tankRX+TAILLE_TANK/2)
        res = 2;

    printf("\nBoulet : X=%i et Y=%i",bouletX,bouletY);
    printf("\nTankBleu : X=%i et Y=%i",tankBX,tankBY);
    printf("\nTankRouge : X=%i et Y=%i",tankRX,tankRY);
    return res;
}


