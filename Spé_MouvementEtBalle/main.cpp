#include <SFML/Graphics.hpp>
#define LONGUEUR_FEN 600
#define HAUTEUR_FEN 900
#define TAILLE_TANK 30
#define PAUSE 10
#define DEGRE 45
#define ROTATION 2
#define PI 3.14156
#include <math.h>
#define TANK_ACC 3
#define BOULET_ACC 5
#define DUREE_BOULET 150

using namespace sf;

// Structure Tank
typedef struct
{
    float posX; // Position en abscisse du tank
    float posY; // Position en ordonn�es du tank
    float rota=0; // Rotation du tank en degre
} Tank;

// On ajoute les signatures des fonctions et proc�dures
void deplacementTankR(Sprite &tankR,Tank &tRouge);
void deplacementTankB(Sprite &tankB,Tank &tBleu);
float convertionDegreRad(float nbr);

int main()
{
    Tank tRouge; // On definit le tank rouge
    Tank tBleu; // On definit le tank bleu
    Tank bBleu;
    Tank bRouge;

    int gestionBouletB = 0;
    int feuBouletB = 0;
    int timeBouletB = 0;

    int gestionBouletR = 0;
    int feuBouletR = 0;
    int timeBouletR = 0;

    tBleu.posX=LONGUEUR_FEN/2; // On initialise la position en abscisse du tank bleu
    tBleu.posY=HAUTEUR_FEN/2; // On initialise la position en ordonnee du tank bleu
    tRouge.posX=LONGUEUR_FEN/3; // On initialise la position en abscisse du tank rouge
    tRouge.posY=HAUTEUR_FEN/3; // On initialise la position en ordonnee du tank rouge

// Cr�ation d'une fenetre
    RenderWindow app(VideoMode(LONGUEUR_FEN, HAUTEUR_FEN), "World of tank");

// On charge l'image du tank rouge dans une texture
    Texture tankrouge;
    if (!tankrouge.loadFromFile("Tank_Rouge.png")) // Si probl�me dans le chargement
        printf("PB de chargement de l'image !\n");

    Sprite tankR;
    tankR.setTexture(tankrouge); // On donne une forme � l'image du tank rouge
    tankR.setPosition(tRouge.posX,tRouge.posY); // On initialise la position du tank rouge
    tankR.setOrigin(TAILLE_TANK/2,TAILLE_TANK/2); // On d�finit la nouvelle origine du tank rouge

// On charge l'image du tank bleu dans une texture
    Texture tankbleu;
    if (!tankbleu.loadFromFile("Tank_Bleu.png")) // Si probl�me dans le chargement
        printf("PB de chargement de l'image !\n");

    Sprite tankB;
    tankB.setTexture(tankbleu); // On donne une forme � l'image du tank bleu
    tankB.setPosition(tBleu.posX,tBleu.posY); // On initialise la position du tank bleu
    tankB.setOrigin(TAILLE_TANK/2,TAILLE_TANK/2); // On d�finit la nouvelle origine du tank bleu

    Texture bouletBleu;
    if (!bouletBleu.loadFromFile("Boulet.png")) // Si probl�me dans le chargement
        printf("PB de chargement de l'image !\n");

    Sprite bouletB;
    bouletB.setTexture(bouletBleu);

     Texture bouletRouge;
    if (!bouletRouge.loadFromFile("Boulet.png")) // Si probl�me dans le chargement
        printf("PB de chargement de l'image !\n");

    Sprite bouletR;
    bouletR.setTexture(bouletRouge);

    app.draw(tankR); // la fen�tre dessine le tank rouge
    app.draw(tankB); // la fen�tre dessine le tank bleu
    app.display(); // la fen�tre affiche ses dessins

    Event event;
// tant que la fen�tre est ouverte
    while (app.isOpen())
    {
        app.clear(); // la fenetre est effacee
        app.draw(tankR); // la fen�tre dessine le tank rouge
        app.draw(tankB); // la fen�tre dessine le tank bleu

        deplacementTankR(tankR,tRouge); // on appelle la procedure deplacementTankR pour deplacer le tank rouge
        deplacementTankB(tankB,tBleu); // on appelle la procedure deplacementTankB pour deplacer le tank bleu

// tant qu'il y a des �v�nements intercept�s par la fen�tre
        while (app.pollEvent(event))
        {
            if (event.type == Event::Closed) // si fermeture de fen�tre d�clench�
                app.close();
            if((event.key.code == Keyboard::Space)&&(timeBouletB == 0)){
                gestionBouletB = 1;
            }
            if((event.key.code == Keyboard::RControl)&&(timeBouletR == 0)){
                gestionBouletR = 1;
            }
        //tireBouletsR(app, tRouge);
            /*if(afficheBoulet()==1)
            {
                posXBouletsB+=10;
                posYBouletsB+=10;
                Boulet.setPosition(posXBouletsB,posYBouletsB);
            }*/

        }

        /**/ /**/ /**/ /**BOULET BLEU**/ /**/ /**/ /**/
        //Gestion du boulet bleu
        if(gestionBouletB == 1){
            bBleu.posX = tBleu.posX;
            bBleu.posY = tBleu.posY;
            bBleu.rota = tBleu.rota;
            feuBouletB = 1;
            gestionBouletB = 0;
        }
        else{
            bouletB.setPosition(0,0);
        }

        if(feuBouletB == 1){
            bBleu.posX+=cos(convertionDegreRad(bBleu.rota + 90*3))*BOULET_ACC;
            bBleu.posY+=sin(convertionDegreRad(bBleu.rota + 90*3))*BOULET_ACC;

            bouletB.setPosition(bBleu.posX,bBleu.posY);

            if(timeBouletB >= DUREE_BOULET){
                timeBouletB = 0;
                feuBouletB = 0;
            }else{
                timeBouletB++;
            }
        }
        /**/ /**/ /**/

        app.draw(bouletB);

        /**/ /**/ /**/ /**BOULET ROUGE**/ /**/ /**/ /**/
        //Gestion du boulet rouge
        if(gestionBouletR == 1){
            bRouge.posX = tRouge.posX;
            bRouge.posY = tRouge.posY;
            bRouge.rota = tRouge.rota;
            feuBouletR = 1;
            gestionBouletR = 0;
        }
        else{
            bouletR.setPosition(0,0);
        }

        if(feuBouletR == 1){
            bRouge.posX+=cos(convertionDegreRad(bRouge.rota + 90*3))*BOULET_ACC;
            bRouge.posY+=sin(convertionDegreRad(bRouge.rota + 90*3))*BOULET_ACC;

            bouletR.setPosition(bRouge.posX,bRouge.posY);

            if(timeBouletR >= DUREE_BOULET){
                timeBouletR = 0;
                feuBouletR = 0;
            }else{
                app.draw(bouletR);
                timeBouletR++;
            }
        }
        /**/ /**/ /**/

        app.draw(tankB);
        app.draw(tankR);
        app.display(); // la fen�tre affiche ses dessins
        sleep(milliseconds(PAUSE)); // on fait une pause
    }
    return EXIT_SUCCESS;
}

float convertionDegreRad(float nbr) // Fonction pour convertir des degres en radians
{
    return nbr /360*2*PI;
}

void deplacementTankR(Sprite &tankR,Tank &tRouge) // procedure pour deplacer le tank rouge
{
    if (Keyboard::isKeyPressed(Keyboard::Up)) // Si la fleche du haut est pressee
    {
        tRouge.posX+=cos(convertionDegreRad(tRouge.rota + 90*3))*TANK_ACC;
        tRouge.posY+=sin(convertionDegreRad(tRouge.rota + 90*3))*TANK_ACC;
        tankR.setPosition(tRouge.posX,tRouge.posY); // On applique la nouvelle position au tank rouge
    }

    if(Keyboard::isKeyPressed(Keyboard::Down)) // Si la fleche du bas est pressee
    {
        tRouge.posX+=cos(convertionDegreRad(tRouge.rota + 90))*TANK_ACC;
        tRouge.posY+=sin(convertionDegreRad(tRouge.rota + 90))*TANK_ACC;
        tankR.setPosition(tRouge.posX,tRouge.posY); // On applique la nouvelle position au tank rouge
    }

    if(Keyboard::isKeyPressed(Keyboard::Right)) // Si la fleche droite est pressee
    {
        tRouge.rota+=5; // on augmente la rotation du tank rouge de 5 degre
        tankR.setRotation(tRouge.rota);
    }

    if(Keyboard::isKeyPressed(Keyboard::Left)) // Si la fleche gauche est pressee
    {
        tRouge.rota-=5; // on diminue la rotation du tank rouge de 5 degre
        tankR.setRotation(tRouge.rota); // On applique la rotation au tank rouge (sens des aiguilles d'une montre)
    }
    if(tRouge.rota>=360) // Si la rotation du tank rouge est >=0 alors on repasse sa valeur � 0 pour rester dans un cercle de 360�
        tRouge.rota=0;
}

void deplacementTankB(Sprite &tankB,Tank &tBleu) // procedure pour deplacer le tank bleu
{
    if (Keyboard::isKeyPressed(Keyboard::Z)) // Si la touche Z est pressee
    {
        tBleu.posX+=cos(convertionDegreRad(tBleu.rota + 90*3))*TANK_ACC;
        tBleu.posY+=sin(convertionDegreRad(tBleu.rota + 90*3))*TANK_ACC;
        tankB.setPosition(tBleu.posX,tBleu.posY);
    }

    if(Keyboard::isKeyPressed(Keyboard::S)) // Si la touche S est pressee
    {
        tBleu.posX+=cos(convertionDegreRad(tBleu.rota + 90))*TANK_ACC;
        tBleu.posY+=sin(convertionDegreRad(tBleu.rota + 90))*TANK_ACC;
        tankB.setPosition(tBleu.posX,tBleu.posY);
    }

    if(Keyboard::isKeyPressed(Keyboard::D)) // Si la touche D est pressee
    {
        tBleu.rota+=5; // on augmente la rotation du tank bleu de 5 degre
        tankB.setRotation(tBleu.rota); // On applique la rotation au tank bleu (sens des aiguilles d'une montre)
    }

    if(Keyboard::isKeyPressed(Keyboard::Q)) // Si la touche Q est pressee
    {
        tBleu.rota-=5; // on diminue la rotation du tank bleu de 5 degre
        tankB.setRotation(tBleu.rota); // On applique la rotation au tank bleu (sens des aiguilles d'une montre)
    }

    if(tBleu.rota>=360) // Si la rotation du tank bleu est >=0 alors on repasse sa valeur � 0 pour rester dans un cercle de 360�
        tBleu.rota=0;
}

/*void tireBallesR(RenderWindow &app, Tank &tRouge)
{
    CircleShape balle(3);
    balle.setFillColor(Color::White);
    balle.setPosition(tRouge.posX,tRouge.posY);
    app.draw(balle);
}
*/

/*void tireBouletsB(RenderWindow &app, Tank &tBleu)
{
    float posXBouletsB=tBleu.posX;
    float posYBouletsB=tBleu.posY;
    Texture boulet;
    if (!boulet.loadFromFile("Boulet.png")) // Si probl�me dans le chargement
        printf("PB de chargement de l'image !\n");

    Sprite Boulet;
    Boulet.setTexture(boulet); // On donne une forme � l'image du tank rouge
    Boulet.setPosition(posXBouletsB,posYBouletsB); // On initialise la position du tank rouge

    if(Keyboard::isKeyPressed(Keyboard::Space))
    {

        posXBouletsB+=10;
        posYBouletsB+=10;
        Boulet.setPosition(posXBouletsB,posYBouletsB);

    }
    app.draw(Boulet);
}*/
/*

Clock clock;
Time elapsed1 = clock.getElapsedTime();

    if(clock.getElapsedTime()<=60)
        {
            app.draw(balle);
            printf("chrono : ", clock.getElapsedTime());
        }
        else
            clock.restart();
            printf("chrono : ", clock.getElapsedTime());
*/
