#include "mur.hpp"
#include "deplacement.hpp"
#include "score.hpp"
using namespace sf;

int main()
{
    Tank tRouge; // On definit le tank rouge
    Tank tBleu; // On definit le tank bleu

    int scoreR=0;
    int scoreB=0;

    tBleu.posX=9*LONGUEUR_FENETRE/10; // On initialise la position en abscisse du tank bleu
    tBleu.posY=8*LARGEUR_FENETRE/10; // On initialise la position en ordonnee du tank bleu
    tRouge.posX=3*LONGUEUR_FENETRE/10; // On initialise la position en abscisse du tank rouge
    tRouge.posY=2*LARGEUR_FENETRE/10; // On initialise la position en ordonnee du tank rouge

// Cr�ation d'une fenetre
    RenderWindow app(VideoMode(LONGUEUR_FENETRE, LARGEUR_FENETRE), "World of tank");


// On charge l'image du tank rouge dans une texture
    Texture tankrouge;
    if (!tankrouge.loadFromFile(SOURCE_TANK_ROUGE)) // Si probl�me dans le chargement
        printf("PB de chargement de l'image !\n");

    RectangleShape tankR(Vector2f(TAILLE_TANK,TAILLE_TANK));
    tankR.setTexture(&tankrouge); // On donne une forme � l'image du tank rouge
    tankR.setPosition(tRouge.posX,tRouge.posY); // On initialise la position du tank rouge
    tankR.setOrigin(TAILLE_TANK/2,TAILLE_TANK/2); // On d�finit la nouvelle origine du tank rouge

// On charge l'image du tank bleu dans une texture
    Texture tankbleu;
    if (!tankbleu.loadFromFile(SOURCE_TANK_BLEU)) // Si probl�me dans le chargement
        printf("PB de chargement de l'image !\n");

    RectangleShape tankB(Vector2f(TAILLE_TANK,TAILLE_TANK));
    tankB.setTexture(&tankbleu); // On donne une forme � l'image du tank bleu
    tankB.setPosition(tBleu.posX,tBleu.posY); // On initialise la position du tank bleu
    tankB.setOrigin(TAILLE_TANK/2,TAILLE_TANK/2); // On d�finit la nouvelle origine du tank bleu

    Texture sol,mur,tankTexture;
    sol.loadFromFile(SOURCE_SOL);
    mur.loadFromFile(SOURCE_MUR);

    RectangleShape grille[NB_CASE_LONGUEUR][NB_CASE_LARGEUR]; //Cr�er les composants de la matrice

    creerGrille(grille,sol);

    chargerCarte(grille,1,mur);

    Event event;
// tant que la fen�tre est ouverte
    while (app.isOpen())
    {

        deplacementTankR(tankR,tRouge,grille,mur); // on appelle la procedure deplacementTankR pour deplacer le tank rouge
        deplacementTankB(tankB,tBleu,grille,mur); // on appelle la procedure deplacementTankB pour deplacer le tank bleu
// tant qu'il y a des �v�nements intercept�s par la fen�tre
        while (app.pollEvent(event))
        {


            if (event.type == Event::Closed) // si fermeture de fen�tre d�clench�
                app.close();
        }
        app.clear(); // la fenetre est effacee
        grilleDraw(grille,app,tankTexture);
        app.draw(tankR); // la fen�tre dessine le tank rouge
        app.draw(tankB); // la fen�tre dessine le tank bleu
        afficheScores(app,scoreR,scoreB);
        app.display(); // la fen�tre affiche ses dessins
        sleep(milliseconds(PAUSE)); // on fait une pause
    }
    return EXIT_SUCCESS;
}
