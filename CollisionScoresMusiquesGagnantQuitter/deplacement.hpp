#include <SFML/Graphics.hpp>
#include <stdlib.h>
#include "mur.hpp"
#define TAILLE_TANK 30
#define PAUSE 10
#define DEGRE 45
#define ROTATION 2
#define PI 3.14156
#include <math.h>
#define TANK_ACC 7

using namespace sf;
// On ajoute les signatures des fonctions et proc�dures

// Structure Tank
typedef struct
{
    float posX; // Position en abscisse du tank
    float posY; // Position en ordonn�es du tank
    float rota=0; // Rotation du tank en degre
} Tank;

void deplacementTankR(RectangleShape &tankR,Tank &tRouge,RectangleShape grille[][NB_CASE_LARGEUR],Texture &mur);

void deplacementTankB(RectangleShape &tankB,Tank &tBleu,RectangleShape grille[][NB_CASE_LARGEUR],Texture &mur);

float convertionDegreRad(float nbr);
