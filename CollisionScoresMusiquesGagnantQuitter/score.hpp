#include <SFML/Graphics.hpp>
#include <stdlib.h>
using namespace sf;

void afficheScores(RenderWindow &app, int &scoreR, int &scoreB);
Font chargeFont1();
Font chargeFont2();
void quitterPartie();
void compareScore(RenderWindow &app, int &scoreB, int &scoreR);
void sonExplosion();
void sonFond();
void sonMenu();
