#include "mur.hpp"

void creerMurH(int posxOrigine, int posxFin, int posy, RectangleShape grille[][NB_CASE_LARGEUR],Texture &texture)
{
    int x,y;
     for(x = 0; x<NB_CASE_LONGUEUR; x++)
        {
            for(y = 0; y<NB_CASE_LARGEUR; y++)
            {
                if(x >= posxOrigine && x <= posxFin && y == posy)
                    grille[x][y].setTexture(&texture);
            }
        }
}

void creerMurV(int posyOrigine, int posyFin, int posx, RectangleShape grille[][NB_CASE_LARGEUR],Texture &texture)
{
    int x,y;
     for(x = 0; x<NB_CASE_LONGUEUR; x++)
        {
            for(y = 0; y<NB_CASE_LARGEUR; y++)
            {
                if(y >= posyOrigine && y <= posyFin && x == posx)
                    grille[x][y].setTexture(&texture);
            }
        }
}

void creerGrille(RectangleShape grille[][NB_CASE_LARGEUR],Texture &texture)
{
    int x,y;

    for(x = (ORIGINE_X_CARTE)/TAILLE_CASE; x<NB_CASE_LONGUEUR+ORIGINE_X_CARTE/TAILLE_CASE; x++)
        {
            for(y = 0; y<NB_CASE_LARGEUR; y++)
            {
                grille[x-(ORIGINE_X_CARTE)/TAILLE_CASE][y].setSize(Vector2f(TAILLE_CASE, TAILLE_CASE));
                grille[x-(ORIGINE_X_CARTE)/TAILLE_CASE][y].setPosition(x*TAILLE_CASE,y*TAILLE_CASE);
                grille[x-(ORIGINE_X_CARTE)/TAILLE_CASE][y].setTexture(&texture);
            }
        }
}

void grilleDraw(RectangleShape grille[][NB_CASE_LARGEUR], RenderWindow &fenetre,Texture &texture)
{
    int x,y;
    for(x = 0; x<NB_CASE_LONGUEUR; x++)
            for(y = 0; y<NB_CASE_LARGEUR; y++)
                if(grille[x][y].getFillColor() == Color::White)
                    fenetre.draw(grille[x][y]);
}

void chargerCarte(RectangleShape grille[][NB_CASE_LARGEUR], int numCarte,Texture &texture)
{
    printf("nbXCase = %i\nnbYCase = %i",NB_CASE_LONGUEUR,NB_CASE_LARGEUR);
    if (numCarte == 1)
    {
        creerMurV(0,NB_CASE_LARGEUR,0,grille,texture);
        creerMurH(0,NB_CASE_LONGUEUR,0,grille,texture);
        creerMurV(0,NB_CASE_LARGEUR,NB_CASE_LONGUEUR-1,grille,texture);
        creerMurH(0,NB_CASE_LONGUEUR,NB_CASE_LARGEUR-1,grille,texture);

        creerMurH(NB_CASE_LONGUEUR/10, 3*NB_CASE_LONGUEUR/10,NB_CASE_LARGEUR/10,grille,texture);
        creerMurV(NB_CASE_LARGEUR/10,3*NB_CASE_LARGEUR/10,2*NB_CASE_LONGUEUR/10,grille,texture);
        creerMurH(2*NB_CASE_LONGUEUR/10, 4*NB_CASE_LONGUEUR/10,2*NB_CASE_LARGEUR/10,grille,texture);

        creerMurH(0*NB_CASE_LONGUEUR/10, 3*NB_CASE_LONGUEUR/10,5*NB_CASE_LARGEUR/10,grille,texture);
        creerMurV(4*NB_CASE_LARGEUR/10,6*NB_CASE_LARGEUR/10,5*NB_CASE_LONGUEUR/10,grille,texture);
        creerMurH(7*NB_CASE_LONGUEUR/10, NB_CASE_LONGUEUR,5*NB_CASE_LARGEUR/10,grille,texture);

        creerMurV(3*NB_CASE_LARGEUR/10,7*NB_CASE_LARGEUR/10,3*NB_CASE_LONGUEUR/10,grille,texture);
        creerMurH(NB_CASE_LONGUEUR/10, 3*NB_CASE_LONGUEUR/10,9*NB_CASE_LARGEUR/10,grille,texture);
        creerMurV(7*NB_CASE_LARGEUR/10,9*NB_CASE_LARGEUR/10,2*NB_CASE_LONGUEUR/10,grille,texture);
        creerMurH(2*NB_CASE_LONGUEUR/10, 4*NB_CASE_LONGUEUR/10,8*NB_CASE_LARGEUR/10,grille,texture);
        creerMurV(3*NB_CASE_LARGEUR/10,7*NB_CASE_LARGEUR/10,7*NB_CASE_LONGUEUR/10,grille,texture);

        creerMurH(7*NB_CASE_LONGUEUR/10, 9*NB_CASE_LONGUEUR/10,NB_CASE_LARGEUR/10,grille,texture);
        creerMurV(NB_CASE_LARGEUR/10,3*NB_CASE_LARGEUR/10,8*NB_CASE_LONGUEUR/10,grille,texture);
        creerMurH(6*NB_CASE_LONGUEUR/10, 8*NB_CASE_LONGUEUR/10,2*NB_CASE_LARGEUR/10,grille,texture);

        creerMurH(7*NB_CASE_LONGUEUR/10, 9*NB_CASE_LONGUEUR/10,9*NB_CASE_LARGEUR/10,grille,texture);
        creerMurV(7*NB_CASE_LARGEUR/10,9*NB_CASE_LARGEUR/10,8*NB_CASE_LONGUEUR/10,grille,texture);
        creerMurH(6*NB_CASE_LONGUEUR/10, 8*NB_CASE_LONGUEUR/10,8*NB_CASE_LARGEUR/10,grille,texture);
        creerMurH(0*NB_CASE_LONGUEUR/10, 3*NB_CASE_LONGUEUR/10,5*NB_CASE_LARGEUR/10,grille,texture);
    }
}

int collision(int posx, int posy, RectangleShape grille[][NB_CASE_LARGEUR],Texture &mur)
{
    int res = 0;
    int numCaseY = (posy)/TAILLE_CASE;
    int numCaseX = (posx)/TAILLE_CASE-ORIGINE_X_CARTE/TAILLE_CASE;


    if((grille[numCaseX][numCaseY].getTexture() == &mur)&&(grille[numCaseX][numCaseY+1].getTexture() == &mur)) //gauhe
        res = 1;
    else if (grille[numCaseX][numCaseY-1].getTexture() == &mur)
        res = 1;
    else if (grille[numCaseX][numCaseY].getTexture() == &mur)
        res = 1;
    else if (grille[numCaseX][numCaseY+1].getTexture() == &mur)
        res = 1;

    if((grille[numCaseX+1][numCaseY-1].getTexture() == &mur)&&(grille[numCaseX+1][numCaseY+1].getTexture() == &mur))  //droite
        res = 2;
    else if (grille[numCaseX+1][numCaseY-1].getTexture() == &mur)
        res = 2;
    else if (grille[numCaseX+1][numCaseY].getTexture() == &mur)
        res = 2;
    else if (grille[numCaseX+1][numCaseY+1].getTexture() == &mur)
        res = 2;

    if((grille[numCaseX-1][numCaseY+1].getTexture() == &mur)&&(grille[numCaseX-1][numCaseY+1].getTexture() == &mur)) //bas
        res = 3;
    else if (grille[numCaseX-1][numCaseY+1].getTexture() == &mur)
        res = 3;
    else if (grille[numCaseX][numCaseY+1].getTexture() == &mur)
        res = 3;
    else if (grille[numCaseX-1][numCaseY+1].getTexture() == &mur)
        res = 3;

    if((grille[numCaseX-1][numCaseY-1].getTexture() == &mur)&&(grille[numCaseX+1][numCaseY-1].getTexture() == &mur)) //haut
        res = 4;
    else if (grille[numCaseX-1][numCaseY-1].getTexture() == &mur)
        res = 4;
    else if (grille[numCaseX][numCaseY-1].getTexture() == &mur)
        res = 4;
    else if (grille[numCaseX+1][numCaseY-1].getTexture() == &mur)
        res = 4;

    return res;
}

