#include "score.hpp"
#define PAUSE2 7
void afficheScores(RenderWindow &app, int &scoreR, int &scoreB)
{
    Font font1 = chargeFont1();
    Font font2 = chargeFont2();

    Text textJoueurR, textJoueurB, textScore, textScoreR, textScoreB;

    textScore.setString("Scores");
    textScore.setFont(font1);
    textScore.setPosition(130,10);
    textScore.setStyle(Text::Underlined);
    textScore.setCharacterSize(40);

    char ch1[5];
    textScoreR.setFont(font2);
    textScoreR.setPosition(10,100);
    textScoreR.setCharacterSize(30);
    sprintf(ch1,"Tank rouge : %i",scoreR);
    textScoreR.setString(ch1);

    char ch2[5];
    textScoreB.setFont(font2);
    textScoreB.setPosition(10,200);
    textScoreB.setCharacterSize(30);
    sprintf(ch2,"Tank bleu : %i",scoreB);
    textScoreB.setString(ch2);


    if(Keyboard::isKeyPressed(Keyboard::L))
    {
        scoreR++;
    }

    if(Keyboard::isKeyPressed(Keyboard::J))
    {
        scoreB++;
    }

    sprintf(ch1,"Tank rouge : %i",scoreR);
    textScoreR.setString(ch1);
    sprintf(ch2,"Tank bleu : %i",scoreB);
    textScoreB.setString(ch2);

    app.draw(textScore);
    app.draw(textJoueurB);
    app.draw(textJoueurR);
    app.draw(textScoreB);
    app.draw(textScoreR);

}
void compareScore(RenderWindow &app, int &scoreB, int &scoreR)
{
    if(scoreB!=scoreR)
    {
        if(scoreR>scoreB)
        {
            Texture gagneR;
            if (!gagneR.loadFromFile("ROUGE_WIN.png")) // Si probl�me dans le chargement
                printf("PB de chargement de l'image gagnant tank rouge !\n");

            Sprite winR;
            winR.setTexture(gagneR); // On donne une forme � l'image du tank rouge
            winR.setPosition(0,0); // On initialise la position du tank rouge
            app.clear();
            app.draw(winR);
        }
        else
        {
            Texture gagneB;
            if (!gagneB.loadFromFile("BLEU_WIN.png")) // Si probl�me dans le chargement
                printf("PB de chargement de l'image gagnant tank bleu !\n");

            Sprite winB;
            winB.setTexture(gagneB); // On donne une forme � l'image du tank rouge
            winB.setPosition(0,0); // On initialise la position du tank rouge
            app.clear();
            app.draw(winB);
        }
    }
}

Font chargeFont1()
{
    Font f ;
    if (!f.loadFromFile("GillSansUltraBold.ttf"))
    {
        printf("pb chargement police");
        exit(1);

    }
    return f;
}

Font chargeFont2()
{
    Font f ;
    if (!f.loadFromFile("ARLRDBD.TTF"))
    {
        printf("pb chargement police");
        exit(1);

    }
    return f;
}


void quitterPartie();
{
    if(Keyboard::isKeyPressed(Keyboard::Escape))
    {
        compareScore(RenderWindow &app, int &scoreB, int &scoreR);
        sleep(milliseconds(PAUSE2)); // on fait une pause
        accueil();
    }

void sonExplosion()
{
    SoundBuffer son1;
    if (!buffer.loadFromFile("explosion.wav"))
        printf("Pb chargement explosion")

        Sound explosion;
    explosion.setBuffer(son1);
    explosion.play();
}

void sonFond()
{
    SoundBuffer son2;
    if (!buffer.loadFromFile("fond.wav"))
        printf("Pb chargement musique de fond")

        Sound fond;
    fond.setBuffer(son2);
    fond.play();
}

void sonMenu()
{
    SoundBuffer son3;
    if (!buffer.loadFromFile("menu.wav"))
        printf("Pb chargement musique menu")

    Sound menu;
    menu.setBuffer(son3);
    menu.play();
}


