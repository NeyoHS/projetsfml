#include "deplacement.hpp"
#include "mur.hpp"

float convertionDegreRad(float nbr) // Fonction pour convertir des degres en radians
{
    return nbr /360*2*PI;
}

void deplacementTankR(RectangleShape &tankR,Tank &tRouge,RectangleShape grille[][NB_CASE_LARGEUR],Texture &mur) // procedure pour deplacer le tank rouge
{
    if (Keyboard::isKeyPressed(Keyboard::Up)) // Si la fleche du haut est pressee
    {
        if(collision(tRouge.posX+cos(convertionDegreRad(tRouge.rota + 90*3))*TANK_ACC,tRouge.posY+sin(convertionDegreRad(tRouge.rota + 90*3))*TANK_ACC,grille,mur) == 0)
        {
            tRouge.posX+=cos(convertionDegreRad(tRouge.rota + 90*3))*TANK_ACC;
            tRouge.posY+=sin(convertionDegreRad(tRouge.rota + 90*3))*TANK_ACC;
            tankR.setPosition(tRouge.posX,tRouge.posY); // On applique la nouvelle position au tank rouge
        }
    }

    if(Keyboard::isKeyPressed(Keyboard::Down)) // Si la fleche du bas est pressee
    {
        if(collision(tRouge.posX+cos(convertionDegreRad(tRouge.rota + 90))*TANK_ACC,tRouge.posY+sin(convertionDegreRad(tRouge.rota + 90))*TANK_ACC,grille,mur) == 0)
        {
            tRouge.posX+=cos(convertionDegreRad(tRouge.rota + 90))*TANK_ACC;
            tRouge.posY+=sin(convertionDegreRad(tRouge.rota + 90))*TANK_ACC;
            tankR.setPosition(tRouge.posX,tRouge.posY); // On applique la nouvelle position au tank rouge
        }
    }

    if(Keyboard::isKeyPressed(Keyboard::Right)) // Si la fleche droite est pressee
    {
        tRouge.rota+=5; // on augmente la rotation du tank rouge de 5 degre
        tankR.setRotation(tRouge.rota);
    }

    if(Keyboard::isKeyPressed(Keyboard::Left)) // Si la fleche gauche est pressee
    {
        tRouge.rota-=5; // on diminue la rotation du tank rouge de 5 degre
        tankR.setRotation(tRouge.rota); // On applique la rotation au tank rouge (sens des aiguilles d'une montre)
    }
    if(tRouge.rota>=360) // Si la rotation du tank rouge est >=0 alors on repasse sa valeur � 0 pour rester dans un cercle de 360�
        tRouge.rota=0;
}

void deplacementTankB(RectangleShape &tankB,Tank &tBleu,RectangleShape grille[][NB_CASE_LARGEUR],Texture &mur) // procedure pour deplacer le tank bleu
{
    if (Keyboard::isKeyPressed(Keyboard::Z)) // Si la touche Z est pressee
    {
        if(collision(tBleu.posX+cos(convertionDegreRad(tBleu.rota + 90*3))*TANK_ACC,tBleu.posY+sin(convertionDegreRad(tBleu.rota + 90*3))*TANK_ACC,grille,mur) == 0)
        {
            tBleu.posX+=cos(convertionDegreRad(tBleu.rota + 90*3))*TANK_ACC;
            tBleu.posY+=sin(convertionDegreRad(tBleu.rota + 90*3))*TANK_ACC;
            tankB.setPosition(tBleu.posX,tBleu.posY);
        }

    }

    if(Keyboard::isKeyPressed(Keyboard::S)) // Si la touche S est pressee
    {
        if(collision(tBleu.posX+cos(convertionDegreRad(tBleu.rota + 90))*TANK_ACC,tBleu.posY+sin(convertionDegreRad(tBleu.rota + 90))*TANK_ACC,grille,mur) == 0)
        {
            tBleu.posX+=cos(convertionDegreRad(tBleu.rota + 90))*TANK_ACC;
            tBleu.posY+=sin(convertionDegreRad(tBleu.rota + 90))*TANK_ACC;
            tankB.setPosition(tBleu.posX,tBleu.posY);
        }
    }

    if(Keyboard::isKeyPressed(Keyboard::D)) // Si la touche D est pressee
    {
        tBleu.rota+=5; // on augmente la rotation du tank bleu de 5 degre
        tankB.setRotation(tBleu.rota); // On applique la rotation au tank bleu (sens des aiguilles d'une montre)
    }

    if(Keyboard::isKeyPressed(Keyboard::Q)) // Si la touche Q est pressee
    {
        tBleu.rota-=5; // on diminue la rotation du tank bleu de 5 degre
        tankB.setRotation(tBleu.rota); // On applique la rotation au tank bleu (sens des aiguilles d'une montre)
    }

    if(tBleu.rota>=360) // Si la rotation du tank bleu est >=0 alors on repasse sa valeur � 0 pour rester dans un cercle de 360�
        tBleu.rota=0;
}
