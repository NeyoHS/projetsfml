#include "score.hpp"

void changeScoreB(RenderWindow &app, Text &textScoreB, int *scoreB, char ch2[])
{
    *scoreB++;
    sprintf(ch2,"Tank bleu : %i",scoreB);
    textScoreB.setString(ch2);
    printf("%s, %i\n",ch2,*scoreB);

}
void changeScoreR(RenderWindow &app, Text &textScoreR, int *scoreR, char ch1[])
{
    *scoreR++;
    sprintf(ch1,"Tank rouge : %i",scoreR);
    textScoreR.setString(ch1);
    printf("%s, %i\n",ch1,*scoreR);


}
void compareScore(RenderWindow &app, int &scoreB, int &scoreR)
{
    if(scoreB!=scoreR)
    {
        if(scoreR>scoreB)
        {
            Texture gagneR;
            if (!gagneR.loadFromFile("ROUGE_WIN.png")) // Si probl�me dans le chargement
                printf("PB de chargement de l'image gagnant tank rouge !\n");

            Sprite winR;
            winR.setTexture(gagneR); // On donne une forme � l'image du tank rouge
            winR.setPosition(0,0); // On initialise la position du tank rouge
            app.clear();
            app.draw(winR);
        }
        else
        {
            Texture gagneB;
            if (!gagneB.loadFromFile("BLEU_WIN.png")) // Si probl�me dans le chargement
                printf("PB de chargement de l'image gagnant tank bleu !\n");

            Sprite winB;
            winB.setTexture(gagneB); // On donne une forme � l'image du tank rouge
            winB.setPosition(0,0); // On initialise la position du tank rouge
            app.clear();
            app.draw(winB);
        }
    }
}

Font chargeFont1()
{
    Font f ;
    if (!f.loadFromFile("GillSansUltraBold.ttf"))
    {
        printf("pb chargement police");
        exit(1);

    }
    return f;
}

Font chargeFont2()
{
    Font f ;
    if (!f.loadFromFile("ARLRDBD.TTF"))
    {
        printf("pb chargement police");
        exit(1);

    }
    return f;
}

/*
void quitterPartie()
{
    if(Keyboard::isKeyPressed(Keyboard::Escape))
    {
        compareScore(&app, &scoreB, &scoreR);
        sleep(milliseconds(PAUSE2)); // on fait une pause
        accueil();
    }
}

    void sonExplosion()
    {
        SoundBuffer son1;
        if (!son1.loadFromFile("explosion.wav"))
            printf("Pb chargement explosion");

            Sound explosion;
        explosion.setBuffer(son1);
        explosion.play();
    }
*/

