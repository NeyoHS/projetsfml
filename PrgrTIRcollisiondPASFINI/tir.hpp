#include <SFML/Graphics.hpp>
#include "mur.hpp"

#define BOULET_ACC 8
#define DUREE_BOULET 150
#define SOURCE_BOULET "Images-Projet\\Boulet.png"
#define TAILLE_BOULET 6

using namespace sf;

int collisionBoulet(int posx, int posy, RectangleShape grille[][NB_CASE_LARGEUR],Texture &mur,Texture &sol);

int touche(int bouletX,int bouletY,int tankBX,int tankBY,int tankRX,int tankRY);
