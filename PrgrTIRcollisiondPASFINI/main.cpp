#include "mur.hpp"
#include "deplacement.hpp"
#include "tir.hpp"
#include "score.hpp"

using namespace sf;

int main()
{
    Tank tRouge; // On definit le tank rouge
    Tank tBleu; // On definit le tank bleu
    Tank bRouge;
    Tank bBleu;

    int gestionBouletB = 0;
    int feuBouletB = 0;
    int timeBouletB = 0;

    int gestionBouletR = 0;
    int feuBouletR = 0;
    int timeBouletR = 0;

    int pasXBouletBleu;
    int pasYBouletBleu;
    int pasXBouletRouge;
    int pasYBouletRouge;

    int scoreR=0;
    int scoreB=0;

    tBleu.posX=9*LONGUEUR_FENETRE/10; // On initialise la position en abscisse du tank bleu
    tBleu.posY=8*LARGEUR_FENETRE/10; // On initialise la position en ordonnee du tank bleu
    tRouge.posX=3*LONGUEUR_FENETRE/10; // On initialise la position en abscisse du tank rouge
    tRouge.posY=2*LARGEUR_FENETRE/10; // On initialise la position en ordonnee du tank rouge

// Cr�ation d'une fenetre
    RenderWindow app(VideoMode(LONGUEUR_FENETRE, LARGEUR_FENETRE), "World of tank");

// On charge l'image du tank rouge dans une texture
    Texture tankrouge;
    tankrouge.loadFromFile(SOURCE_TANK_ROUGE); // Si probl�me dans le chargement

    RectangleShape tankR(Vector2f(TAILLE_TANK,TAILLE_TANK));
    tankR.setTexture(&tankrouge); // On donne une forme � l'image du tank rouge
    tankR.setPosition(tRouge.posX,tRouge.posY); // On initialise la position du tank rouge
    tankR.setOrigin(TAILLE_TANK/2,TAILLE_TANK/2); // On d�finit la nouvelle origine du tank rouge

// On charge l'image du tank bleu dans une texture
    Texture tankbleu;
    tankbleu.loadFromFile(SOURCE_TANK_BLEU); // Si probl�me dans le chargement

    RectangleShape tankB(Vector2f(TAILLE_TANK,TAILLE_TANK));
    tankB.setTexture(&tankbleu); // On donne une forme � l'image du tank bleu
    tankB.setPosition(tBleu.posX,tBleu.posY); // On initialise la position du tank bleu
    tankB.setOrigin(TAILLE_TANK/2,TAILLE_TANK/2); // On d�finit la nouvelle origine du tank bleu

    Texture boulet;
    boulet.loadFromFile(SOURCE_BOULET);
    Sprite bouletR, bouletB;
    bouletR.setTexture(boulet);
    bouletB.setTexture(boulet);

    Texture sol,mur,tankTexture;
    sol.loadFromFile(SOURCE_SOL);
    mur.loadFromFile(SOURCE_MUR);

    RectangleShape grille[NB_CASE_LONGUEUR][NB_CASE_LARGEUR]; //Cr�er les composants de la matrice

    Font font1 = chargeFont1();
    Font font2 = chargeFont2();

    Text textJoueurR, textJoueurB, textScore, textScoreR, textScoreB;

    textScore.setString("Scores");
    textScore.setFont(font1);
    textScore.setPosition(130,10);
    textScore.setStyle(Text::Underlined);
    textScore.setCharacterSize(40);

    char ch1[50];
    textScoreR.setFont(font2);
    textScoreR.setPosition(10,100);
    textScoreR.setCharacterSize(30);
    sprintf(ch1,"Tank rouge : %i",scoreR);
    textScoreR.setString(ch1);

    char ch2[50];
    textScoreB.setFont(font2);
    textScoreB.setPosition(10,200);
    textScoreB.setCharacterSize(30);
    sprintf(ch2,"Tank bleu : %i",scoreB);
    textScoreB.setString(ch2);

    creerGrille(grille,sol);

    chargerCarte(grille,1,mur);

    Event event;
// tant que la fen�tre est ouverte
    while (app.isOpen())
    {
        deplacementTankR(tankR,tRouge,grille,mur); // on appelle la procedure deplacementTankR pour deplacer le tank rouge
        deplacementTankB(tankB,tBleu,grille,mur); // on appelle la procedure deplacementTankB pour deplacer le tank bleu
// tant qu'il y a des �v�nements intercept�s par la fen�tre
        while (app.pollEvent(event))
        {
            if (event.type == Event::Closed) // si fermeture de fen�tre d�clench�
                app.close();

            if((event.key.code == Keyboard::Space)&&(timeBouletB == 0))
            {
                gestionBouletB = 1;
            }


            if((event.key.code == Keyboard::RControl)&&(timeBouletR == 0))
                gestionBouletR = 1;
        }

        if(gestionBouletB == 1)
        {
            bBleu.rota = tBleu.rota;
            bBleu.posX = tBleu.posX + ((TAILLE_TANK/2)+TAILLE_CASE)*cos(convertionDegreRad(bBleu.rota + 90*3));
            bBleu.posY = tBleu.posY + ((TAILLE_TANK/2)+TAILLE_CASE)*sin(convertionDegreRad(bBleu.rota + 90*3));
            pasXBouletBleu = cos(convertionDegreRad(bBleu.rota + 90*3))*BOULET_ACC;
            pasYBouletBleu = sin(convertionDegreRad(bBleu.rota + 90*3))*BOULET_ACC;
            feuBouletB = 1;
            gestionBouletB = 0;
        }
        else
        {
            bouletB.setPosition(0,0);
        }

        if(feuBouletB == 1)
        {
            if(collisionBoulet(bBleu.posX,bBleu.posY,grille,mur,sol) == 1)
                pasXBouletBleu=-pasXBouletBleu;
            if(collisionBoulet(bBleu.posX,bBleu.posY,grille,mur,sol) == 2)
                pasXBouletBleu=-pasXBouletBleu;
            if(collisionBoulet(bBleu.posX,bBleu.posY,grille,mur,sol) == 3)
                pasYBouletBleu=-pasYBouletBleu;
            if(collisionBoulet(bBleu.posX,bBleu.posY,grille,mur,sol) == 4)
                pasYBouletBleu=-pasYBouletBleu;

            bBleu.posX+=pasXBouletBleu;
            bBleu.posY+=pasYBouletBleu;
            bouletB.setPosition(bBleu.posX,bBleu.posY);

             if(touche(bBleu.posX,bBleu.posY,tBleu.posX,tBleu.posY,tRouge.posX,tRouge.posY) == 1)
            {
                timeBouletB = DUREE_BOULET;
                changeScoreR(app, textScoreR, &scoreR, ch1);
                //ins�rer code score avec joueur rouge gagnant
                printf("\nContact1");
                tBleu.posX=9*LONGUEUR_FENETRE/10; // On initialise la position en abscisse du tank bleu
                tBleu.posY=8*LARGEUR_FENETRE/10; // On initialise la position en ordonnee du tank bleu
                tRouge.posX=3*LONGUEUR_FENETRE/10; // On initialise la position en abscisse du tank rouge
                tRouge.posY=2*LARGEUR_FENETRE/10; // On initialise la position en ordonnee du tank rouge
                tankB.setPosition(tBleu.posX,tBleu.posY);
                tankR.setPosition(tRouge.posX,tRouge.posY);
            }
            else if(touche(bBleu.posX,bBleu.posY,tBleu.posX,tBleu.posY,tRouge.posX,tRouge.posY) == 2)
            {
                timeBouletB = DUREE_BOULET;
                changeScoreB(app, textScoreB, &scoreB, ch2);
                //ins�rer code score avec joueur bleu gagnant
                printf("\nContac2t");
                tBleu.posX=9*LONGUEUR_FENETRE/10; // On initialise la position en abscisse du tank bleu
                tBleu.posY=8*LARGEUR_FENETRE/10; // On initialise la position en ordonnee du tank bleu
                tRouge.posX=3*LONGUEUR_FENETRE/10; // On initialise la position en abscisse du tank rouge
                tRouge.posY=2*LARGEUR_FENETRE/10; // On initialise la position en ordonnee du tank rouge
                tankB.setPosition(tBleu.posX,tBleu.posY);
                tankR.setPosition(tRouge.posX,tRouge.posY);
            }

            if(timeBouletB >= DUREE_BOULET)
            {
                timeBouletB = 0;
                feuBouletB = 0;
            }
            else
            {
                timeBouletB++;
            }
        }

        if(gestionBouletR == 1)
        {
            bRouge.rota = tRouge.rota;
            bRouge.posX = tRouge.posX + ((TAILLE_TANK/2)+TAILLE_CASE)*cos(convertionDegreRad(bRouge.rota + 90*3));
            bRouge.posY = tRouge.posY + ((TAILLE_TANK/2)+TAILLE_CASE)*sin(convertionDegreRad(bRouge.rota + 90*3));
            pasXBouletRouge = cos(convertionDegreRad(bRouge.rota + 90*3))*BOULET_ACC;
            pasYBouletRouge = sin(convertionDegreRad(bRouge.rota + 90*3))*BOULET_ACC;
            feuBouletR = 1;
            gestionBouletR = 0;
        }
        else
            bouletR.setPosition(0,0);

        if(feuBouletR == 1)
        {
            if(collisionBoulet(bRouge.posX,bRouge.posY,grille,mur,sol) == 1)
                pasXBouletRouge=-pasXBouletRouge;
            if(collisionBoulet(bRouge.posX,bRouge.posY,grille,mur,sol) == 2)
                pasXBouletRouge=-pasXBouletRouge;
            if(collisionBoulet(bRouge.posX,bRouge.posY,grille,mur,sol) == 3)
                pasYBouletRouge=-pasYBouletRouge;
            if(collisionBoulet(bRouge.posX,bRouge.posY,grille,mur,sol) == 4)
                pasYBouletRouge=-pasYBouletRouge;

            bRouge.posY+=pasYBouletRouge;
            bRouge.posX+=pasXBouletRouge;
            bouletR.setPosition(bRouge.posX,bRouge.posY);

            if(touche(bRouge.posX,bRouge.posY,tBleu.posX,tBleu.posY,tRouge.posX,tRouge.posY) == 1)
            {
                timeBouletR = DUREE_BOULET;

                changeScoreR(app, textScoreR, &scoreR, ch1);
                //ins�rer code score avec joueur rouge gagnant
                printf("\nContact3");
                tBleu.posX=9*LONGUEUR_FENETRE/10; // On initialise la position en abscisse du tank bleu
                tBleu.posY=8*LARGEUR_FENETRE/10; // On initialise la position en ordonnee du tank bleu
                tRouge.posX=3*LONGUEUR_FENETRE/10; // On initialise la position en abscisse du tank rouge
                tRouge.posY=2*LARGEUR_FENETRE/10; // On initialise la position en ordonnee du tank rouge
                tankB.setPosition(tBleu.posX,tBleu.posY);
                tankR.setPosition(tRouge.posX,tRouge.posY);
            }
            else if(touche(bRouge.posX,bRouge.posY,tBleu.posX,tBleu.posY,tRouge.posX,tRouge.posY) == 2)
            {
                timeBouletR = DUREE_BOULET;
                changeScoreB(app, textScoreB, &scoreB, ch2);
                //ins�rer code score avec joueur bleu gagnant
                printf("\nContact4");
                tBleu.posX=9*LONGUEUR_FENETRE/10; // On initialise la position en abscisse du tank bleu
                tBleu.posY=8*LARGEUR_FENETRE/10; // On initialise la position en ordonnee du tank bleu
                tRouge.posX=3*LONGUEUR_FENETRE/10; // On initialise la position en abscisse du tank rouge
                tRouge.posY=2*LARGEUR_FENETRE/10; // On initialise la position en ordonnee du tank rouge
                tankB.setPosition(tBleu.posX,tBleu.posY);
                tankR.setPosition(tRouge.posX,tRouge.posY);
            }

            if(timeBouletR >= DUREE_BOULET)
            {
                timeBouletR = 0;
                feuBouletR = 0;
            }
            else
                timeBouletR++;
        }




        app.clear(); // la fenetre est effacee
        grilleDraw(grille,app,tankTexture);
        app.draw(tankR); // la fen�tre dessine le tank rouge
        app.draw(tankB); // la fen�tre dessine le tank bleu
        app.draw(textScore);
        app.draw(textJoueurB);
        app.draw(textJoueurR);
        app.draw(textScoreB);
        app.draw(textScoreR);
        app.draw(bouletR);
        app.draw(bouletB);
        app.display(); // la fen�tre affiche ses dessins
        sleep(milliseconds(PAUSE)); // on fait une pause
    }
    return EXIT_SUCCESS;
}
