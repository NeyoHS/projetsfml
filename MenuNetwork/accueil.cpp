#include "mur.hpp"
#include "deplacement.hpp"
#include <iostream>
#include <sstream>
#include <SFML/Network.hpp>
#include <SFML/Graphics.hpp>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include "accueil.hpp"

//Define des constantes de taille
#define LARG_FEN 1600
#define HAUT_FEN 900
#define LARG_MAIN 700
#define HAUT_MAIN 200
#define LARG_BOUTON 500
#define HAUT_BOUTON 100
#define ECART_BOUTON 150
#define PAUSE 10
/*
using namespace sf;


const unsigned short PORT = 5000;
const IpAddress IPADDRESS("10.103.60.9"); //change to suit your needs

TcpSocket socket;
Mutex globalMutex;
bool quit = false;

//using namespace std;

//Structure de position
typedef struct
{
    int x;
    int y;
} Position;



void Server(void)
{
    TcpListener listener;
    listener.listen(PORT);
    listener.accept(socket);
    cout << "New client connected: " << socket.getRemoteAddress() << endl;
}

bool Client(void)
{
    if(socket.connect(IPADDRESS, PORT) == Socket::Done)
    {
        cout << "Connected\n";
        return true;
    }
    return false;
}

int accueil()
{
//Rectangle de selection des menus
    RectangleShape rect(Vector2f(LARG_BOUTON+20, HAUT_BOUTON+20));
    rect.setFillColor(Color::White);

//Process des events
    Event event;

//Initialisation de la position des menus
    Position coordSouris = {0,0};
    Position coordMainMenu = {(LARG_FEN/2)-(LARG_MAIN/2),100};
    Position coordLocalMenu = {(LARG_FEN/2)-(LARG_BOUTON/2),HAUT_MAIN+ECART_BOUTON};
    Position coordMultiMenu = {(LARG_FEN/2)-(LARG_BOUTON/2),HAUT_MAIN+ECART_BOUTON*2};
    Position coordHebergerMenu = {(LARG_FEN/2)-(LARG_BOUTON/2),HAUT_MAIN+ECART_BOUTON};
    Position coordRejoindreMenu = {(LARG_FEN/2)-(LARG_BOUTON/2),HAUT_MAIN+ECART_BOUTON*2};
    Position coordRetourMenu = {(LARG_FEN/2)-(LARG_BOUTON/2),HAUT_MAIN+ECART_BOUTON*3};


//Variables de gestion des menus
int choixMenu = 1;
int afficheMenuDeux = 0;
int setRect = 0;




// Fenetre
    RenderWindow app(VideoMode(LARG_FEN, HAUT_FEN), "World of tanks");
    app.clear(Color::Black);

// Chargement des images du menu
    Texture imageMainMenu;
    imageMainMenu.loadFromFile("Main_Menu.png");
    Texture imageLocalMenu;
    imageLocalMenu.loadFromFile("Local_Menu.png");
    Texture imageMultiMenu;
    imageMultiMenu.loadFromFile("Multi_Menu.png");
    Texture imageHebergerMenu;
    imageHebergerMenu.loadFromFile("Heberger_Menu.png");
    Texture imageRejoindreMenu;
    imageRejoindreMenu.loadFromFile("Rejoindre_Menu.png");
    Texture imageBackgroundMenu;
    imageBackgroundMenu.loadFromFile("Background_menu.jfif");
    Texture imageRetour;
    imageRetour.loadFromFile("Local_Menu.png");

//Cr�ation des sprites
    Sprite spriteMainMenu;
    spriteMainMenu.setTexture(imageMainMenu);
    Sprite spriteLocalMenu;
    spriteLocalMenu.setTexture(imageLocalMenu);
    Sprite spriteMultiMenu;
    spriteMultiMenu.setTexture(imageMultiMenu);
    Sprite spriteHebergerMenu;
    spriteHebergerMenu.setTexture(imageHebergerMenu);
    Sprite spriteRejoindreMenu;
    spriteRejoindreMenu.setTexture(imageRejoindreMenu);
    Sprite spriteBackgroundMenu;
    spriteBackgroundMenu.setTexture(imageBackgroundMenu);
    spriteBackgroundMenu.setScale(1.5625,1.171875);
    Sprite spriteRetour;
    spriteRetour.setTexture(imageRetour);

//Position des sprites
    spriteMainMenu.setPosition(coordMainMenu.x,coordMainMenu.y);
    spriteLocalMenu.setPosition(coordLocalMenu.x,coordLocalMenu.y);
    spriteMultiMenu.setPosition(coordMultiMenu.x,coordMultiMenu.y);
    spriteHebergerMenu.setPosition(coordHebergerMenu.x,coordHebergerMenu.y);
    spriteRejoindreMenu.setPosition(coordRejoindreMenu.x,coordRejoindreMenu.y);
    spriteRetour.setPosition(coordRetourMenu.x,coordRetourMenu.y);

//Boucle du jeu
    while (app.isOpen())
    {
        while (app.pollEvent(event))
        {
            switch(event.type)
            {

            //Event de fermeture de la fenetre
            case Event::Closed:
                app.close();

            case Event::KeyPressed:
            {
                //Events de defilement dans les menus
                if((event.key.code == Keyboard::Down)&&(choixMenu<3))
                {
                    choixMenu++;
                    printf("%i ",choixMenu);
                }

                if((event.key.code == Keyboard::Up)&&(choixMenu>1))
                {
                    choixMenu--;
                    printf("%i ",choixMenu);
                }

                //Event de selections des menus
                if(event.key.code == Keyboard::Return)
                {
                    //Selection Jeu en local
                    if(choixMenu == 1 && afficheMenuDeux == 0)
                    {
                        //
                        //  JEU EN LOCAL
                        //
                        printf("local ");
                    }

                    //Selection multijoueur
                    else if(choixMenu == 2 && afficheMenuDeux == 0)
                    {
                        afficheMenuDeux = 1;
                    }

                    //Selection heberger
                    else if ((choixMenu == 1) && (afficheMenuDeux == 1))
                    {
                        //
                        //  JEU EN HEBERGEUR
                        //
                        printf("hebergement ");
                        Server();

                    }

                    //Selection rejoindre
                    else if((choixMenu == 2) && (afficheMenuDeux == 1))
                    {
                        //
                        //  REJOINDRE UN JEU
                        //
                        printf("rejoindre ");
                        Client();

                    }

                    //Retour depuis menu multijoueur
                    else if ((choixMenu == 3) && (afficheMenuDeux == 1))
                    {
                        afficheMenuDeux = 0;
                        choixMenu = 1;
                        printf("retour ");
                    }

                    //Quitter le jeu
                    else if ((choixMenu == 3) && (afficheMenuDeux == 0))
                    {
                        app.close();
                        printf("exit ");
                    }
                }
            }

            //Event de gestion de la position de la souris
            case Event::MouseMoved:
            {
                coordSouris.x = event.mouseMove.x;
                coordSouris.y = event.mouseMove.y;
            }

            }
        }

        //Placement du rectangle de choix
        if(choixMenu == 2)
        {
            setRect = 2;
        }
        else if(choixMenu == 1)
        {
            setRect = 1;
        }
        else if(choixMenu == 3)
        {
            setRect = 3;
        }

        if  (setRect == 1)
        {
            rect.setPosition(coordLocalMenu.x-10, coordLocalMenu.y-10);
        }
        else if (setRect == 2)
        {
            rect.setPosition(coordMultiMenu.x-10, coordMultiMenu.y-10);
        }
        else if (setRect == 3)
        {
            rect.setPosition(coordRetourMenu.x-10, coordRetourMenu.y-10);
        }

        app.draw(spriteBackgroundMenu);
        app.draw(rect);
        app.draw(spriteMainMenu);
        app.draw(spriteLocalMenu);
        app.draw(spriteMultiMenu);
        if (afficheMenuDeux == 1)
        {
            app.draw(spriteHebergerMenu);
            app.draw(spriteRejoindreMenu);
        }
        app.draw(spriteRetour);
        app.display();
        sleep( milliseconds(PAUSE));
        app.clear();
    }
    printf("%i ",choixMenu);
}*/



