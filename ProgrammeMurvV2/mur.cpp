#include "mur.hpp"

void creerMurH(int posxOrigine, int posxFin, int posy, RectangleShape grille[][NB_CASE_LARGEUR])
{
    int x,y;
     for(x = 0; x<NB_CASE_LONGUEUR; x++)
        {
            for(y = 0; y<NB_CASE_LARGEUR; y++)
            {
                if(x >= posxOrigine && x <= posxFin && y == posy)
                    grille[x][y].setFillColor(Color::White);
            }
        }
}

void creerMurV(int posyOrigine, int posyFin, int posx, RectangleShape grille[][NB_CASE_LARGEUR])
{
    int x,y;
     for(x = 0; x<NB_CASE_LONGUEUR; x++)
        {
            for(y = 0; y<NB_CASE_LARGEUR; y++)
            {
                if(y >= posyOrigine && y <= posyFin && x == posx)
                    grille[x][y].setFillColor(Color::White);
            }
        }
}

void creerGrille(RectangleShape grille[][NB_CASE_LARGEUR])
{
    int x,y;

    for(x = (ORIGINE_X_CARTE)/TAILLE_CASE; x<NB_CASE_LONGUEUR+ORIGINE_X_CARTE/TAILLE_CASE; x++)
        {
            for(y = 0; y<NB_CASE_LARGEUR; y++)
            {
                grille[x-(ORIGINE_X_CARTE)/TAILLE_CASE][y].setSize(Vector2f(TAILLE_CASE, TAILLE_CASE));
                grille[x-(ORIGINE_X_CARTE)/TAILLE_CASE][y].setPosition(x*TAILLE_CASE,y*TAILLE_CASE);
                grille[x-(ORIGINE_X_CARTE)/TAILLE_CASE][y].setFillColor(Color::Black);
            }
        }
}

void grilleDraw(RectangleShape grille[][NB_CASE_LARGEUR], RenderWindow &fenetre)
{
    int x,y;
    for(x = 0; x<NB_CASE_LONGUEUR; x++)
            for(y = 0; y<NB_CASE_LARGEUR; y++)
                if((grille[x][y].getFillColor() == Color::White) || (grille[x][y].getFillColor() == Color::Blue))
                    fenetre.draw(grille[x][y]);
}

void chargerCarte(RectangleShape grille[][NB_CASE_LARGEUR], int numCarte)
{
    printf("nbXCase = %i\nnbYCase = %i",NB_CASE_LONGUEUR,NB_CASE_LARGEUR);
    if (numCarte == 1)
    {
        creerMurV(0,NB_CASE_LARGEUR,0,grille);
        creerMurH(0,NB_CASE_LONGUEUR,0,grille);
        creerMurV(0,NB_CASE_LARGEUR,NB_CASE_LONGUEUR-1,grille);
        creerMurH(0,NB_CASE_LONGUEUR,NB_CASE_LARGEUR-1,grille);

        creerMurV(0,NB_CASE_LARGEUR/10,NB_CASE_LONGUEUR/10,grille);
        creerMurH(NB_CASE_LONGUEUR/10, 3*NB_CASE_LONGUEUR/10,NB_CASE_LARGEUR/10,grille);
        creerMurV(NB_CASE_LARGEUR/10,3*NB_CASE_LARGEUR/10,2*NB_CASE_LONGUEUR/10,grille);
        creerMurH(2*NB_CASE_LONGUEUR/10, 4*NB_CASE_LONGUEUR/10,2*NB_CASE_LARGEUR/10,grille);

        creerMurV(4*NB_CASE_LARGEUR/10,6*NB_CASE_LARGEUR/10,5*NB_CASE_LONGUEUR/10,grille);

        creerMurV(9*NB_CASE_LARGEUR/10,NB_CASE_LARGEUR,NB_CASE_LONGUEUR/10,grille);
        creerMurH(NB_CASE_LONGUEUR/10, 3*NB_CASE_LONGUEUR/10,9*NB_CASE_LARGEUR/10,grille);
        creerMurV(7*NB_CASE_LARGEUR/10,9*NB_CASE_LARGEUR/10,2*NB_CASE_LONGUEUR/10,grille);
        creerMurH(2*NB_CASE_LONGUEUR/10, 4*NB_CASE_LONGUEUR/10,8*NB_CASE_LARGEUR/10,grille);

        creerMurV(0,NB_CASE_LARGEUR/10,9*NB_CASE_LONGUEUR/10,grille);
        creerMurH(7*NB_CASE_LONGUEUR/10, 9*NB_CASE_LONGUEUR/10,NB_CASE_LARGEUR/10,grille);
        creerMurV(NB_CASE_LARGEUR/10,3*NB_CASE_LARGEUR/10,8*NB_CASE_LONGUEUR/10,grille);
        creerMurH(6*NB_CASE_LONGUEUR/10, 8*NB_CASE_LONGUEUR/10,2*NB_CASE_LARGEUR/10,grille);

        creerMurV(9*NB_CASE_LARGEUR/10,NB_CASE_LARGEUR,9*NB_CASE_LONGUEUR/10,grille);
        creerMurH(7*NB_CASE_LONGUEUR/10, 9*NB_CASE_LONGUEUR/10,9*NB_CASE_LARGEUR/10,grille);
        creerMurV(7*NB_CASE_LARGEUR/10,9*NB_CASE_LARGEUR/10,8*NB_CASE_LONGUEUR/10,grille);
        creerMurH(6*NB_CASE_LONGUEUR/10, 8*NB_CASE_LONGUEUR/10,8*NB_CASE_LARGEUR/10,grille);
    }
}

int collision(int posx, int posy, RectangleShape grille[][NB_CASE_LARGEUR])
{
    int res = 0;
    int numCaseY = (posy)/TAILLE_CASE;
    int numCaseX = (posx)/TAILLE_CASE-ORIGINE_X_CARTE/TAILLE_CASE;


    if((grille[numCaseX][numCaseY].getFillColor() == Color::White)&&(grille[numCaseX][numCaseY+1].getFillColor() == Color::White)) //gauhe
        res = 1;
    if((grille[numCaseX+2][numCaseY].getFillColor() == Color::White)&&(grille[numCaseX+2][numCaseY+1].getFillColor() == Color::White))  //droite
        res = 2;
    if((grille[numCaseX][numCaseY+2].getFillColor() == Color::White)&&(grille[numCaseX+1][numCaseY+2].getFillColor() == Color::White)) //bas
        res = 3;
    if((grille[numCaseX][numCaseY].getFillColor() == Color::White)&&(grille[numCaseX+1][numCaseY].getFillColor() == Color::White)) //haut
        res = 4;



    return res;
}

