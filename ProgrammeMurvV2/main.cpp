#include "mur.hpp"

typedef struct
{
    int x,y;
} Point;


int main()
{
    RenderWindow fenetre(VideoMode(LONGUEUR_FENETRE, LARGEUR_FENETRE), "SFML");
    fenetre.setFramerateLimit(60);

    RectangleShape grille[NB_CASE_LONGUEUR][NB_CASE_LARGEUR]; //Cr�er les composants de la matrice
    creerGrille(grille); //Cr�er la matrice
    chargerCarte(grille,1);

    RectangleShape tank(Vector2f(TAILLE_TANK,TAILLE_TANK));
    tank.setFillColor(Color::Green);
    Point tankPos = {1400,100};

    int pasx = 3, pasy = 3;

    while (fenetre.isOpen())
    {
        Event event;
        while (fenetre.pollEvent(event))
        {
            if (event.type == Event::Closed)
                fenetre.close();
        }

        if(collision(tankPos.x,tankPos.y,grille) == 1)
            pasx = -pasx;
        else if(collision(tankPos.x,tankPos.y,grille) == 2)
            pasx = -pasx;

        if(collision(tankPos.x,tankPos.y,grille) == 3)
            pasy = -pasy;
        else if(collision(tankPos.x,tankPos.y,grille) == 4)
            pasy = -pasy;

        tankPos.x+=pasx;
        tankPos.y+=pasy;

        tank.setPosition(tankPos.x,tankPos.y);

        fenetre.clear(Color::Black);

        grilleDraw(grille,fenetre);

        fenetre.draw(tank);
        fenetre.display();
    }

    return EXIT_SUCCESS;
}
